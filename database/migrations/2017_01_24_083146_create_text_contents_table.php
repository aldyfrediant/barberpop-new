<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('text_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('page');
            $table->text('content');
            $table->timestamps();
        });

       DB::table('text_contents')->insert([
        'page' => 'Home - Lookbook Left Text',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor facilisis sodales.'
       ]);

       DB::table('text_contents')->insert([
        'page' => 'Home - Lookbook Right Text',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor facilisis sodales.'
       ]);

       DB::table('text_contents')->insert([
        'page' => 'About',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor facilisis sodales.'
       ]);

       DB::table('text_contents')->insert([
        'page' => 'Shop - Left Text',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor facilisis sodales.'
       ]);

       DB::table('text_contents')->insert([
        'page' => 'Shop - Right Text',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor facilisis sodales.'
       ]);

       DB::table('text_contents')->insert([
        'page' => 'Franchise',
        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor facilisis sodales.'
       ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('text_contents');
    }
}
