<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
//Back-Admin Route
Route::get('dashboard', function () {
    return redirect('dashboard/index');
});
Route::group(['prefix' => 'dashboard'], function () {
	Route::resource('index','AdminController');
    Route::resource('text', 'AdminTextController');
		Route::get('/text/{id}/edit', 'AdminTextController@edit')->name('text-edit');
	Route::resource('image', 'AdminImageController');
		Route::get('/image/{id}/edit', 'AdminImageController@edit')->name('image-edit');
	Route::resource('social', 'AdminSocialController');
		Route::get('/social/{id}/edit', 'AdminSocialController@edit')->name('social-edit');
	Route::resource('user','UserController');
	Route::resource('message', 'AdminMessageController');
	Route::resource('store', 'AdminStoreController');
		Route::get('/store/{id}/edit', 'AdminStoreController@edit')->name('store-edit');
});

//Front Route
Route::get('/', 'HomeController@home');
Route::get('/home', 'HomeController@home');
Route::get('/about', 'HomeController@about');
Route::get('/prices-and-services', 'HomeController@prices');
Route::get('/shop', 'HomeController@shop');
Route::get('/gallery', 'HomeController@gallery');
Route::get('/location', 'HomeController@location');
Route::get('/contact', 'HomeController@contact');
	Route::post('message','HomeController@submit_message');
Route::get('/franchise', 'HomeController@franchise');

//Auth
Route::Auth();
Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\Logincontroller@logout');
Route::post('logout', 'Auth\LoginController@logout');