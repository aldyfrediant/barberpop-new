<!DOCTYPE html>
    <head>
        <meta charset="utf-8">

        <title>{{ config('app.title') }}</title>

        <meta name="description" content="AppUI is a Web App Bootstrap Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ URL::asset('assets/img/favicon.png') }}">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/plugins.css') }}">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css') }}">->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/themes.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/themes/passion.css') }}">
        <!-- END Stylesheets -->

        {{-- Custom Styles --}}
        <link rel="stylesheet" href="{{ URL::asset('assets/css/custom.css') }}">

        <!-- Modernizr (browser feature detection library) -->
        <script src="{{ URL::asset('assets/js/vendor/modernizr-3.3.1.min.js') }}"></script>
        @yield('addCSS')
    </head>
    <body>
        <div id="page-wrapper" class="page-loading">
            <div class="preloader">
                <div class="inner">
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
                </div>
            </div>
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                @include('admin.layouts.sidebar')

                <div id="main-container">

                    @include('admin.layouts.header')

                    @yield('content')

                </div>
            </div>
        </div>

        <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
        <script src="{{ URL::asset('assets/js/vendor/jquery-2.2.4.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('assets/js/plugins.js') }}"></script>
        <script src="{{ URL::asset('assets/js/app.js') }}"></script>

        <!-- ckeditor.js, load it only in the page you would like to use CKEditor (it's a heavy plugin to include it with the others!) -->
        <script src="{{ URL::asset('assets/js/plugins/ckeditor/ckeditor.js') }}"></script>

        @yield('addJS')
    </body>
</html>