<!-- Main Sidebar -->
<div id="sidebar">
    <!-- Sidebar Brand -->
    <div id="sidebar-brand" class="themed-background">
        <a href="{{ URL::to('adm') }}" class="sidebar-title">
            <i class="fa fa-scissors"></i> <span class="sidebar-nav-mini-hide">Barberpop<strong>Inc.</strong></span>
        </a>
    </div>
    <!-- END Sidebar Brand -->
    <!-- Wrapper for scrolling functionality -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Sidebar Navigation -->
            <ul class="sidebar-nav">
                <li>
                    <a href="{{ URL::to('adm') }}" class="@if($code == 1) active @endif"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                </li>
                <li class="sidebar-separator">
                    <i class="fa fa-ellipsis-h"></i>
                </li>
                <li>
                    <a href="{{ URL::to('adm/text') }}" class="@if($code == 2) active @endif"><i class="gi gi-align_left sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Text Content</span></a>
                </li>
                <li>
                    <a href="{{ URL::to('adm/image') }}" class="@if($code == 3) active @endif"><i class="gi gi-paperclip sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Image Content</span></a>
                </li>
                <li>
                    <a href="{{ URL::to('adm/social') }}" class="@if($code == 4) active @endif"><i class="gi gi-globe sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Social Media</span></a>
                </li>
                <li>
                    <a href="{{ URL::to('adm/store') }}" class="@if($code == 5) active @endif"><i class="gi gi-google_maps sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Store Location</span></a>
                </li>
                <li class="sidebar-separator">
                    <i class="fa fa-ellipsis-h"></i>
                </li>
                <li>
                    <a href="{{ URL::to('adm/message') }}" class="@if($code == 6) active @endif"><i class="gi gi-message_plus sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Visitor Messages</span></a>
                </li>
            </ul>
            <!-- END Sidebar Navigation -->
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->

    <!-- Sidebar Extra Info -->
    <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
        <div class="text-center">
            <small>2017 &copy; <a href="https://barberpop.co.id" target="_blank">Barberpop</a></small><br>
            <small>Supported by <a href="https://pentacode.id" target="_blank">Pentacode Digital</a></small>
        </div>
    </div>
    <!-- END Sidebar Extra Info -->
</div>
<!-- END Main Sidebar -->