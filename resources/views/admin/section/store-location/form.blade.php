@extends('admin.layouts.app')

@section('addCSS')
<style>
    body{
        overflow-y: hidden;
    }
</style>

@endsection

@section('content')
<!-- Page content -->
    <div id="page-content">
        <!-- Forms Components Header -->
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        @if($action == 'store.update')
                            <h1>{{ $store->name }}</h1>
                        @else
                            <h1>Add New Store</h1>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                        <ul class="breadcrumb breadcrumb-top">
                            <li><a href="{{ URL::to('adm') }}">Home</a></li>
                            <li><a href="{{ URL::to('adm/store') }}">Store Location</a></li>
                            @if($action == 'store.update')
                                <li>Edit Content</li>
                            @else
                                <li>Add New</li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Forms Components Header -->

        <!-- Form Components Row -->
        <div class="row">
            <div class="col-md-12">
                <!-- Horizontal Form Block -->
                <div class="block">
                    <!-- Horizontal Form Content -->
                    @if($action == 'store.store')
                        {!! Form::open(['route' => $action, 'class' => 'form-horizontal form-bordered']) !!}
                    @else
                        {!! Form::model($store, ['route' => [$action, $store->id], 'class'=> 'form-horizontal form-bordered', 'method' => 'PUT']) !!}
                    @endif
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">City</label>
                            <div class="col-md-10">
                                {!! Form::select('city', 
                                    [
                                    'Balikpapan' => 'Balikpapan',
                                    'Bandung' => 'Bandung',
                                    'Batam' => 'Batam',
                                    'Denpasar' => 'Denpasar',
                                    'Jakarta' => 'Jakarta',
                                    'Kendari' => 'Kendari',
                                    'Makassar' => 'Makassar',
                                    'Manado' => 'Manado',
                                    'Medan' => 'Medan',
                                    'Palembang' => 'Palembang',
                                    'Sukabumi' => 'Sukabumi',
                                    ], $store->city, ['class' => 'form-control'])
                                !!}
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">Store Name</label>
                            <div class="col-md-10">
                                {!! Form::text('name', $store->name, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">Store Address</label>
                            <div class="col-md-10">
                                {!! Form::text('address', $store->address, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="example-hf-email">Google Maps URL</label>
                            <div class="col-md-10">
                                {!! Form::text('url', $store->url, ['class' => 'form-control']) !!}
                            </div>
                        </div>                             
                        <div class="form-group form-actions">
                            <div class="col-md-12" style="text-align: right;">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                    <!-- END Horizontal Form Content -->
                </div>
                <!-- END Horizontal Form Block -->
            </div>
        </div>
    </div>
    <!-- END Page Content -->
@endsection