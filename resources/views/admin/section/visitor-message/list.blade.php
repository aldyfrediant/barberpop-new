@extends('admin.layouts.app')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Table Styles Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Visitor Messages</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="{{ URL::to('adm') }}">Home</a></li>
                        <li>Visitor Messages</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END Table Styles Header -->
    <!-- Datatables Block -->
    <!-- Datatables is initialized in js/pages/uiTables.js -->
    <div class="block full">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>E-mail</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Attachment</th>
                        <th width="80px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($message as $data)
                    {!! Form::open([ 'method' => 'delete', 'route' => ['message.destroy', $data->id]]) !!}
                    <tr>
                        <td>{{ $data->email }}</td>
                        <td>{{ $data->subject }}</td>
                        <td>{{ $data->message }}</td>
                        <td>{{ $data->url }}</td>
                        <td style="text-align: center;">
                            <button type="submit" class="btn btn-danger btn-xs btn-icon"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    {!! Form::close() !!}
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Block -->
</div>
<!-- END Page Content -->
@endsection

@section('addJS')
@endsection