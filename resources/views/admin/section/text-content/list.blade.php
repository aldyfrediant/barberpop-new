@extends('admin.layouts.app')

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Table Styles Header -->
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1>Text Contents</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="{{ URL::to('adm') }}">Home</a></li>
                        <li>Text Contents</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END Table Styles Header -->
    <!-- Datatables Block -->
    <!-- Datatables is initialized in js/pages/uiTables.js -->
    <div class="block full">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>Page</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($text as $data)
                    <tr>
                        <td>{{ $data->page }}</td>
                        <td>{{ $data->content }}</td>
                        <td style="text-align: center;"><a class="btn btn-warning btn-xs btn-icon" href="{{ URL::to('adm/text/'.$data->id.'/edit') }}"><i class="fa fa-pencil"></i></a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Block -->
</div>
<!-- END Page Content -->
@endsection

@section('addJS')
    <!-- Load and execute javascript code used only in this page -->
    <script src="{{ URL::asset('assets/js/pages/uiTables.js') }}"></script>
    <script>$(function(){ UiTables.init(); });</script>
@endsection