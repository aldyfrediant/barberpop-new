
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/jq/jquery-ui.css') }}">
</head>
<body>
{!! Form::open(['action' => 'AdminController@xxx']) !!}

{!! Form::text('date', null, ['id' => 'date']) !!}

{!! Form::submit('SUBMIT', []) !!}

{!! Form::close() !!}

<script src="{{ URL::asset('assets/jq/jquery.js') }}"></script>
<script src="{{ URL::asset('assets/jq/jquery-ui.js') }}"></script>
<script type="text/javascript">
	var dateToday = new Date();
	var dates = $("#date").datepicker({
	    defaultDate: "now",
	    changeMonth: true,
	    numberOfMonths: 1,
	    minDate: dateToday,
	    onSelect: function(selectedDate) {
	        var option = this.id == "from" ? "minDate" : "maxDate",
	            instance = $(this).data("datepicker"),
	            date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
	        dates.not(this).datepicker("option", option, date);
	    }
	});
</script>
</body>
</html>