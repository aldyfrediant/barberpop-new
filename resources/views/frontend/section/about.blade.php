@extends('frontend.layouts.app')

@section('content')
<div id="content" class="container">
  <div class="row">
    <div class="col-sm-8">
     <div class="col-sm-12 general-title">
            <h4>ABOUT US</h4>
        </div>
      <div class="col-sm-12 about-content">
        @foreach($content as $data)
              <p>{{$data->content}}</p>
        @endforeach
        <p class="dotdot">.........................</p>
      </div>
    </div>
    <div class="col-sm-4 right-img">
       @foreach($sidebar as $data)
            <div class="sidebar" style="background-image: url('{{URL::to('uploads/image/'.$data->url)}}'); background-size: cover; background-position: center;">
            </div>
        @endforeach
    </div>
  </div>
</div>
@endsection