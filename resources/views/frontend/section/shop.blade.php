@extends('frontend.layouts.app')

@section('content')
<div id="content" class="container" style="height:100%">
  <div class="row">
    <div class="col-sm-12 general-title">
        <h4>SHOP</h4>
    </div>
    <div class="col-sm-3">
      @foreach($conleft as $data)
          <p>{{$data->content}}</p>
      @endforeach
    </div>
    <div class="col-sm-6">
      <div class="row">
      @foreach($product as $data)
        <div class="col-sm-4 col-xs-6 shop-block">
          <img src="{{URL::to('uploads/image/'.$data->url)}}">
          <p>{{$data->caption}}</p>
        </div>
      @endforeach
      </div>
    </div>
    <div class="col-sm-3">
      @foreach($conright as $data)
          <p>{{$data->content}}</p>
      @endforeach
    </div>
  </div>
</div>
@endsection
