@extends('frontend.layouts.app')

@section('content')
<div id="content" class="container" style="height:100%">
  <div class="row gallery">
    <div class="col-sm-12 general-title">
        <h4>GALLERY</h4>
    </div>
    <div class="col-sm-12 colorbox">
    @php $galleryCount = 1; @endphp
    @foreach($image as $data)
        <div class="@if($galleryCount % 5 == 1) @php echo 'col-sm-offset-1'; @endphp @endif
          @php $galleryCount++; @endphp
          col-sm-2 col-xs-4 gallery-block">
          <a class="group1" href="{{URL::to('uploads/image/'.$data->url)}}" title="{{$data->caption}}">
          <img src="{{URL::to('uploads/image/gallery/thumbs-'.$data->url)}}" />
        </a>
      </div>
    @endforeach
    </div>
  </div>
</div>

<!-- Slider Gallery-->
<script src="assets/js/jquery.colorbox.js"></script>
<script>
      $(document).ready(function(){
        $(".group1").colorbox({rel:'group1'});
        $("#click").click(function(){ 
          $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
          return false;
        });
      });
</script>     
@endsection
