@extends('frontend.layouts.app')

@section('content')
<div id="content" class="container" style="height:100%">
  <div class="row">
    <div class="col-sm-12">
      <div class="row">            
        <div class="col-sm-12 general-title">
            <h4>PRICES & SERVICES</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <div class="row">
        @foreach($content as $data)
            <div class="col-sm-3 col-xs-6">
              <div class="product-picture">
                <img src="{{URL::to('uploads/image/'.$data->url)}}" />
              </div>
            </div>
        @endforeach
          </div>
        </div>
        <div class="col-sm-4 right-img">
        @foreach($sidebar as $data)
            <div class="sidebar" style="background-image: url('{{URL::to('uploads/image/'.$data->url)}}'); background-size: cover; background-position: center;">
            </div>
        @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
