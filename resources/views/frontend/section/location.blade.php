@extends('frontend.layouts.app')

@section('content')
<div id="content" class="container" style="height:100%">
  <div class="row">
    <div class="col-sm-8">
      <div class="col-sm-12 general-title">
          <h4>OUR BARBERSHOP</h4>
      </div>

      <div class="col-sm-12">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          
          @php $count = 0 @endphp
          @foreach($store_group as $key=>$barberpop)
            @php
              if($count == 0)
              {
                $expanded = "true";
                $class = "collapse in";
              }
              else
              {
                $expanded = "false";
                $class = "collapse";
              }
            @endphp


          <!-- 1 Item -->
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="heading{{ $key }}">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $key }}" aria-expanded="{{ $expanded }}" aria-controls="{{ $key }}">
                  {{ $key }}
                </a>
              </h4>
            </div>
            <div id="{{ $key }}" class="panel-collapse {{ $class }}" role="tabpanel" aria-labelledby="heading{{ $key }}">
              <div class="panel-body">
                <ul class="list-unstyled">
                @foreach($barberpop as $item)
                  <li style="border-bottom:solid 1px #ccc; padding-bottom: 20px">
                    <h3>{{$item->name}}</h3>  
                    <div>{{$item->address}}</div>
                    <div style="margin-top: 10px">  
                      <a class="btn btn-primary view-store" data="{{ $count++ }}">View Store</a>
                    </div>
                  </li>
                @endforeach
                </ul>

              </div>
            </div>
          </div>
          @endforeach

        </div>
      </div>

    </div>
    <div class="col-sm-4">
      <div id="map" style="width:100%; height: 500px;"></div>
        <script>

          var regex = new RegExp('@(.*),(.*),');
          var count = 0;
          var defaultBarberpop;
          var barberpop = new Array();
          var map;

          $( document ).ready(function() {
              initMap();
          });

          function initMap() {
			@php $count = 0 @endphp
            @foreach($store_group as $barberpop)
				@foreach($barberpop as $data)
				var theAddress{{ $count }} = "{{ $data->url }}";

			  	var lon_lat_match{{ $count }} = theAddress{{ $count }}.match(regex);
			  	var temp = {lat: parseFloat(lon_lat_match{{ $count }}[1]), lng: parseFloat(lon_lat_match{{ $count }}[2]) };
			  	barberpop.push(temp);
			  
			  	@php $count++ @endphp
				@endforeach
            @endforeach

            if(barberpop.length==0)
              //-6.902405,107.6184991
              defaultBarberpop = {lat: -6.902405, lng: 107.6184991};
            else
              defaultBarberpop = barberpop[0];

            map = new google.maps.Map(document.getElementById('map'), {
              zoom: 17,
              center: defaultBarberpop
            });

            @php $count = 0 @endphp
            @foreach($store_group as $barberpop)
				@foreach($barberpop as $data)
				var marker{{ $count }} = new google.maps.Marker({
					position: barberpop[{{ $count }}],
					map: map,
					icon: "{{ URL::asset('assets/img/logo-small.png') }}"
				});
			  
			  	@php $count++ @endphp
			  	@endforeach
            @endforeach
          }

          $(".view-store").click(function(){
            var barberId = $(this).attr("data");
            console.log(barberpop[barberId]);
            console.log(map);
            map.setCenter(barberpop[barberId]);
            console.log(map);
          });
          
        </script>
        <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChvqT5E-miPhO5sGEdNUKlDt_47n0ERU0&callback=initMap">
        </script>
      </body>
    </div>
  </div>
</div>
@endsection
