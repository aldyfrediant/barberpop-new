<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    <link href="{{URL::to('assets/front/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- style -->
    <link href="{{URL::to('assets/front/css/style.css')}}" rel="stylesheet">
    <!-- Colorbox -->
    <link href="{{URL::to('assets/front/css/colorbox.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{URL::to('assets/front/css/custom.css')}}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::to('assets/front/js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{URL::to('js/app.js')}}"></script>
    <script src="{{URL::to('assets/front/bootstrap/js/bootstrap.min.js')}}"></script>
    <!--Twitter Widget-->
    <script async src="{{URL::to('https://platform.twitter.com/widgets.js')}}"></script>
    <!--Instagram Widget-->
    <script src="http://lightwidget.com/widgets/lightwidget.js"></script>
    <title>{{ucfirst($page)}}</title>
    <link rel="icon" href="{{URL::to('assets/front/favicon.ico')}}" type="image/x-icon">
</head>
<body>
    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')
</body>
</html>
