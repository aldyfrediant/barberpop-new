<div id="header" class="container">
    <div class="row">
        <div class="col-sm-8 top-content">
            <span class="logo-block">
                <img src="{{URL::to('assets/front/img/logo.png')}}" class="logo-top" />
            </span>
            <span class="logo-block-responsive">
                <img src="{{URL::to('assets/front/img/logo-responsive.png')}}" class="logo-top" />
            </span>
            <span class="menu-block">
                <div class="menu-icon">
                    <a class="navbar-toggle" data-toggle="collapse" data-target="#topnav" area-expanded="false" href="#">
                        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
                    </a>
                </div>
                <ul class="menu nav navbar-nav navbar-collapse" id="topnav">
                    <li><a href="{{URL::to('/')}}">Home</a></li>
                    <li><a href="{{URL::to('/about')}}">About</a></li>
                    <li><a href="{{URL::to('/prices-and-services')}}">Prices &amp; Services</a></li>
                    <li><a href="{{URL::to('/gallery')}}">Gallery</a></li>
                    <li><a href="{{URL::to('/location')}}">Location</a></li>
                    <li><a href="{{URL::to('/contact')}}">Contact</a></li>
                    <li><a href="{{URL::to('/franchise')}}">Franchise</a></li>
                    <li><a href="//shop.barberpop.co.id">Shop</a></li>
                </ul>
            </span>
        </div>
        <div class="col-sm-4">
            <img src="{{URL::to('assets/front/img/premium-animated.gif')}}" class="logo-premium" />
        </div>
    </div>
</div>