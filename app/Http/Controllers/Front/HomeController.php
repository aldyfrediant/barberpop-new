<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Text;
use App\Image;


class HomeController extends Controller
{
    public function index(){
        $lbleft = DB::table('text_contents')
                ->where('page', 'Home - Lookbook Left Text')
                ->get();
        $lbright = DB::table('text_contents')
                ->where('page', 'Home - Lookbook Right Text')
                ->get();
        $lookbook = DB::table('image_contents')
                ->where('page', 'Home - Lookbook')
                ->limit(4)
                ->orderby('id','DESC')
                ->get();
        $slider = DB::table('image_contents')
                ->where('page', 'Home - Slideshow')
                ->get();
        $sidebar = DB::table('image_contents')
                ->where('page', 'Home - Sidebar')
                ->get();

        $page = "home";
        return view('frontend.section.home')
        	->with('lbleft', $lbleft)
        	->with('lbright', $lbright)
        	->with('lookbook', $lookbook)
        	->with('slider', $slider)
        	->with('sidebar', $sidebar)
        	->with('page', $page);
    }
}
