<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class PnSController extends Controller
{
    public function index(){
    	$content = DB::table('image_contents')
                ->where('page', 'Prices & Services - Pricing')
                ->get();
        $sidebar = DB::table('image_contents')
                ->where('page', 'Prices & Services - Sidebar')
                ->get();
        
        $page = "pricing";
        return view('frontend.section.prices')
        	->with('content', $content)
        	->with('sidebar', $sidebar)
            ->with('page',$page);
    }
}
