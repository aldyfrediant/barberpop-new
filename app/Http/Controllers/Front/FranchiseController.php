<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class FranchiseController extends Controller
{
    public function index(){
        $content = DB::table('text_contents')
                ->where('page', 'Franchise')
                ->get();
                
        $sidebar = DB::table('image_contents')
                ->where('page', 'Franchise - Sidebar')
                ->get();
        
        $page = "franchise";

        return view('frontend.section.franchise')
        	->with('content', $content)
        	->with('sidebar', $sidebar)
            ->with ('page', $page);
    }
}
