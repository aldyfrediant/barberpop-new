<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Store;

class LocationController extends Controller
{
    public function index(){
    	$store = Store::all();
        $barberpop = array();
        foreach($store as $row)
        {
            $barberpop[$row->city][] = $row;
        }

        $page = "location";
        return view('frontend.section.location')
        	->with('store', $store)
        	->with('store_group', $barberpop)
            ->with('page',$page);
    }
}
