<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Store;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $code = 5;
        $store = Store::all();
        return view('admin.section.store-location.list')
            ->with('code', $code)
            ->with('store', $store);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $store = New Store;
        $code = 5;
        $action = 'store.store';
        return view('admin.section.store-location.form')
            ->with('store', $store)
            ->with('code', $code)
            ->with('action', $action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = New Store;

        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'url' => 'required',
        ]);

        $store->name = $request->name;
        $store->address = $request->address;
        $store->city = $request->city;
        $store->url = $request->url;
        $store->save();

        return redirect()->route('store.index');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = Store::find($id);
        $code = 5;
        $action = 'store.update';
        return view('admin.section.store-location.form')
            ->with('store', $store)
            ->with('code', $code)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Store::find($id);

        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'url' => 'required',
        ]);

        $store->name = $request->name;
        $store->address = $request->address;
        $store->city = $request->city;
        $store->url = $request->url;
        $store->save();

        return redirect()->route('store.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Store::destroy($id);
        return redirect()->route('store.index');
    }
}
