<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Social;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $code = 4;
        $social = Social::all();
        return view('admin.section.social-media.list')
            ->with('social', $social)
            ->with('code', $code);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $social = Social::find($id);
        $code = 4;
        return view('admin.section.social-media.form')
            ->with('code', $code)
            ->with('social', $social);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $social = Social::find($id);

        $request->validate([
            'url' => 'required',
        ]);

        $social->url = $request->url;
        $social->save();

        return redirect()->route('social.index');
    }
}
