<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $code = 6;
        $message = Message::all();
        return view('admin.section.visitor-message.list')
            ->with('code', $code)
            ->with('message', $message);
    }

    public function destroy($id)
    {
        Message::destroy($id);
        return redirect()->route('message.index');
    }
}
